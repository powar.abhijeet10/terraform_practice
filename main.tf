# Define provider
provider "aws" {
  region = "us-east-1"  # Update with your desired region
}

# Create security group
resource "aws_security_group" "sentrifugo_sg" {
  name        = "sentrifugo-sg"
  description = "Security group for Sentrifugo"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Create EC2 instance
resource "aws_instance" "sentrifugo_instance" {
  ami           = "ami-0c94855ba95c71c99"  # Ubuntu 20.04 LTS - Update with desired AMI
  instance_type = "t2.micro"                # Update with desired instance type
  key_name      = "your_key_pair"           # Update with your key pair name
  security_group_names = [aws_security_group.sentrifugo_sg.name]
  
  # User data for installation script
  user_data = <<-EOF
              #!/bin/bash
              apt-get update -y
              apt-get install -y apache2

              # Download and install Sentrifugo
              wget -O /tmp/sentrifugo.zip https://www.sentrifugo.com/home/downloadfile/?code=sentrifugo_zip
              unzip /tmp/sentrifugo.zip -d /var/www/html/
              chown -R www-data:www-data /var/www/html/sentrifugo/
              chmod -R 755 /var/www/html/sentrifugo/

              # Start Apache web server
              systemctl start apache2
              systemctl enable apache2
              EOF

  tags = {
    Name = "sentrifugo-instance"
  }
}

# Output instance public IP
output "public_ip" {
  value = aws_instance.sentrifugo_instance.public_ip
}
